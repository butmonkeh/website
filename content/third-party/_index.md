+++
title = "3rd Party"
sort_by = "weight"
template = "sections/third-party.html"
+++

Many of our users contribute to our community by helping with asusctl, supergfxctl, or kernel hacking. However, outside of this we have also had some amazing third party utilities created by users in the community. 

This page will be updated as we become aware of new tools, scripts, and other goodies someone has written to expand the functionality of our tools.

If you also want to share something with us, be sure to check out the #third-party channel in our [Discord](https://discord.gg/z8y99XqPb7) community!