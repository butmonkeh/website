+++
title = "AniMe Matrix - Text Scroller"
weight = 4
+++

A lightweight Python script that allows you to make create simple text scrolling animations
that can be played on your AniMe Matrix.

<video controls muted width="248" height="440">
  <source src="/videos/third-party/anime-textscroller.mp4" type="video/mp4">
</video>

You can download the script here - [https://github.com/x0wllaar/AniMeScroller](https://github.com/x0wllaar/AniMeScroller)