+++
title = "Suspend issues on 2021 G15 with a secondary NVMe drive"
+++

<del>If you have a 2021 G15 and use a second NVMe drive you might experience a problem while using s0ix (s2idle) resulting in a delayed system suspend.

The problem lies within a faulty DSDT table and the secondary NVMe drive does not enter suspend properly.

With an updated DSDT table you can apply the necessary patches to make it suspend properly.

https://gitlab.com/smbruce/GA503QR-StorageD3Enable-DSDT-Patch

**Important:** This is not necessary if you want to use S3 (see above). It applies only if you want to further use the standard suspend method s0ix.</del>

Fixed in kernels versions 6.1.x and up.