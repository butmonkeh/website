+++
title = "Can I customise the Fn key?"
+++

No, the key is on a physically different circuit and used to *physically* signal the keyboard EC to switch key circuits.

There are three different circuits for the `0x8166` keyboard.