+++
title = "I get an error \"org.asuslinux.Daemon was not provided by any .service files\" when I run asusctl"
# template = "faq.html"
+++

The daemon isn't running, check the logs with `sudo journalctl -b -u asusd` and look for errors.